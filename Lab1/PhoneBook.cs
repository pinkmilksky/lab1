﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1
{
    class PhoneBook
    {
        List<Record> Records = new List<Record>();

        public void AddRecord()
        {
            Console.WriteLine("Введите имя: ");
            string firstName = Console.ReadLine();
            Console.WriteLine("Введите фамилию: ");
            string lastName = Console.ReadLine();
            Console.WriteLine("Введите номер телефона: ");
            string phoneNumber = Console.ReadLine();
            Console.WriteLine("Введите страну: ");
            string country = Console.ReadLine();
            Record record = new Record(firstName, lastName, phoneNumber, country);
            Records.Add(record);
            Console.WriteLine("Создан контакт с id: " + record.id);
        }

        public void GetAllRecords()
        {
            foreach (Record rec in Records)
            {
                Console.WriteLine(rec);
            }
        }

        public void DeleteRecord()
        {
            Console.WriteLine("Введите id контакта, которого вы хотите удалить: ");
            int id;
            while (!Int32.TryParse(Console.ReadLine(), out id))
            {
                Console.WriteLine("Введите корректный id");
            }
            foreach (Record rec in Records)
            {
                if (rec.id == id)
                {
                    Records.Remove(rec);
                    Console.WriteLine("Контакт удален");
                    return;

                }
            }
            Console.WriteLine("Элемент с указанным id не найден");
        }

        public void ChangeRecord()
        {
            Console.WriteLine("Введите id контакта, информацию которого вы хотите изменить: ");
            int id;
            while (!Int32.TryParse(Console.ReadLine(), out id))
            {
                Console.WriteLine("Введите корректный id");
            }
            foreach (Record rec in Records)
            {
                if (rec.id == id)
                {
                    Console.WriteLine(rec);
                    //return;
                    bool x = true;
                    while (x == true)
                    {
                        Console.WriteLine("Введите 1, чтобы изменить фамилию: ");
                        Console.WriteLine("Введите 2, чтобы изменить имя: ");
                        Console.WriteLine("Введите 3, чтобы изменить отчество: ");
                        Console.WriteLine("Введите 4, чтобы изменить номер телефона: ");
                        Console.WriteLine("Введите 5, чтобы изменить страну: ");
                        Console.WriteLine("Введите 6, чтобы изменить дату рождения: ");
                        Console.WriteLine("Введите 7, чтобы изменить организацию: ");
                        Console.WriteLine("Введите 8, чтобы изменить должность: ");
                        Console.WriteLine("Введите 9, чтобы добавить доролнительную информацию: ");
                        Console.WriteLine("Введите 10, чтобы завершить изменения");
                        string change = Console.ReadLine();
                        switch (change)
                        {
                            case "1":
                                Console.WriteLine("Введите фамилию: ");
                                rec.lastName = Console.ReadLine();
                                break;
                            case "2":
                                Console.WriteLine("Введите имя: ");
                                rec.firstName = Console.ReadLine();
                                break;
                            case "3":
                                Console.WriteLine("Введите отчество: ");
                                rec.secondName = Console.ReadLine();
                                break;
                            case "4":
                                Console.WriteLine("Введите номер телефона: ");
                                rec.phoneNumber = Console.ReadLine();
                                break;
                            case "5":
                                Console.WriteLine("Введите страну: ");
                                rec.country = Console.ReadLine();
                                break;
                            case "6":
                                Console.WriteLine("Введите дату рождения: ");
                                rec.birthday = Console.ReadLine();
                                break;
                            case "7":
                                Console.WriteLine("Введите организацию: ");
                                rec.organization = Console.ReadLine();
                                break;
                            case "8":
                                Console.WriteLine("Введите должность: ");
                                rec.profession = Console.ReadLine();
                                break;
                            case "9":
                                Console.WriteLine("Введите дополнительную информацию: ");
                                rec.other = Console.ReadLine();
                                break;
                            case "10":
                                x = false;
                                break;

                            default:
                                Console.WriteLine("Вы ввели что-то не то, попробуйте еще раз");
                                break;
                        }


                    }
                }
                else
                {
                    Console.WriteLine("Контакт с указанным id не существует");
                }


            }

        }

        public void FindRecord()
        {
            Console.WriteLine("Введите id контакта, которого вы хотите найти: ");
            int id;
            while (!Int32.TryParse(Console.ReadLine(), out id))
            {
                Console.WriteLine("Введите корректный id");
            }

            foreach (Record rec in Records)
            {
                if (rec.id == id)
                {
                    Console.WriteLine(rec);
                    return;
                }

            }
            Console.WriteLine("Контанкта с таким id не существует");
        }

    }
}
