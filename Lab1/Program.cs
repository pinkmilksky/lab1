﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Lab1
{ 
    class Program
    {
        static void Main(string[] args)
        {
            
            PhoneBook phoneBook = new PhoneBook();
            while (true)
            {
                Console.WriteLine("Введите 1, чтобы добавить контакт");
                Console.WriteLine("Введите 2, чтобы удалить контакт");
                Console.WriteLine("Введите 3, чтобы изменить данные контакта или добавить дополнительную информацию");
                Console.WriteLine("Введите 4, чтобы просмотреть все контакты");
                Console.WriteLine("Введите 5, чтобы найти контакт");
                Console.WriteLine("Введите 6, чтобы выйти");
                string s = Console.ReadLine();
                switch (s)
                {
                    case "1":
                        phoneBook.AddRecord();
                        break;
                    case "2":
                        phoneBook.DeleteRecord();
                        break;
                    case "3":
                        phoneBook.ChangeRecord();
                        break;
                    case "4":
                        phoneBook.GetAllRecords();
                        break;
                    case "5":
                        phoneBook.FindRecord();
                        break;
                    case "6":
                        return;
                    default:
                        Console.WriteLine("Вы ввели что-то не то, попробуйте еще раз");
                        break;
                }
            }
        }
    }
}
