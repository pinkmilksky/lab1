﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1
{
    class Record
    {
        private static int nextId = 0;
        public int id;
        public string lastName;
        public string firstName;
        public string phoneNumber;
        public string country;
        public string secondName;
        public string birthday;
        public string organization;
        public string profession;
        public string other;

        public override string ToString()
        {
            return "Пользователь с id: " + this.id + "\n" + this.lastName + "\n" + this.firstName + "\n" + this.phoneNumber + "\n"
                + this.country + "\n" + this.secondName + "\n" + this.birthday + "\n" + this.organization + "\n"
                + this.profession + "\n" + this.other;
        }

        public Record(string firstName, string lastName, string phoneNumber, string country)
        {
            this.id = nextId++;
            this.firstName = firstName;
            this.lastName = lastName;
            this.phoneNumber = phoneNumber;
            this.country = country;
            secondName = "";
            birthday = "";
            organization = "";
            other = "";
            profession = "";

        }


    }
}
